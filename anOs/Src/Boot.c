
/**
* @brief an os boot file
* @author chenjl
* @date 2019.07.01
*/

#include "OsConfigure.h"
#include "Kernel.h"
#include "KerEvent.h"
#include "kerMpu.h"

uint32_t SystemCoreClock = 168000000;

static void StartOsTimer(void);
static void StartTask(void);
static void OsInit(void);
static uint32 safetyStack[100];

/**
* @brief 板卡初始化
* @details 在系统进入__main之前，需要先打开浮点功能；否则在内部会发生异常
*/
void BoardInit(void)
{
    //在内存拷贝之前，需要先打开浮点功能
    SCB->CPACR |= ((3UL << 10*2)|(3UL << 11*2));  /* set CP10 and CP11 Full Access */
}

/**
* @brief 准备OS运行环境
* @details 进入该函数时，已经完成了对系统时钟的初始化工作
*/
int main(void)
{
    /*加载配置的全局参数信息*/
    LoadConfiguration();
 
    /*设置中断优先级*/
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);	
	NVIC_SetPriority(PendSV_IRQn,255);	    		//PENDSV
	NVIC_SetPriority(SysTick_IRQn,1);				//systick 
    
    /*OS内核初始化*/
    OsInit();
    
    /*启动任务定时器*/
    StartOsTimer();
    
    /*启动某个任务*/
    StartTask();
    return 0;
}

/**
* @brief 启动系统主定时器
*/
static void StartOsTimer(void)
{
    uint32 tickCount = TICK_PERIOD*(SystemCoreClock / 1000000);
    SysTick->LOAD = tickCount - 1;
    SysTick->VAL  = 0;
    SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk |
                   SysTick_CTRL_TICKINT_Msk   |
                   SysTick_CTRL_ENABLE_Msk;
}

/**
* @brief 启动后台任务，在所有的配置任务执行完成后切换执行的任务
*/
static void StartTask(void)
{
   __set_PSP((uint32)(safetyStack+99));
    
    //是否使用MPU保护内存单元
    #if (KERNEL_USE_MPU == 1)	
    kerSetIdleTaskMpu();
    #endif
    //是否区分特权模式和用户模式
    #if (KERNEL_USE_PRIV == 1)
    __set_CONTROL(0x3 | __get_CONTROL());
	#else	
    __set_CONTROL(0x2 | __get_CONTROL());
	#endif
    __ISB();
    __WFI();
    
    while(1)
    {
        //__WFI();        //进入睡眠模式  
        
    }
}

/**
* @brief 操作系统内核初始化模块，负责调用各子系统模块的初始化函数
*/
static void OsInit(void)
{   
    kerEventModuleInit();

    #if (KERNEL_USE_MPU == 1)
    kerMpuMdlInit();
    #endif
    
}

/**
* @brief 切换后台任务执行时，设置其对应的MPU保护区域。
*/
void kerSetIdleTaskMpu(void)
{
    kerMpuResetSetting();
    kerMpuSet((uint32)StartTask,2048,
        (uint32)safetyStack,1024);      
}
