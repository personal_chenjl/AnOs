

/**
* @brief fifo module
* @author chenjl
* @date 2019.07.02
*/

#include "KerFifo.h"

/***
* @brief 使用一段内存，构造成FIFO对象
*/
KERFIFO_HEAD* kerFifo_Create(uint8 *fifobuff,int32 size)
{
    KERFIFO_HEAD *fifo = (KERFIFO_HEAD*)fifobuff;
    if(size > 8)
    {
        fifo->magic = FIFI_MAGIC;
        fifo->rdidx = 0;
        fifo->wtidx = 0;
        fifo->size = size - 8;
    }
    else
    {
        fifo = 0;
    }
    return fifo;
}

/**
* @brief 写入一段FIFO
*/
int32 kerFifo_Write(KERFIFO_HEAD* fifo,uint8 *pdata,int32 len)
{
    int32 cpycnt;
    if(fifo->magic != FIFI_MAGIC)
    {
        //无效的FIFO描述符，直接退出
        return 0;
    }
    for(cpycnt = 0; cpycnt < len; cpycnt++)
    {
        fifo->data[fifo->wtidx] = pdata[cpycnt];
        fifo->wtidx = (fifo->wtidx + 1) % fifo->size;
    }
    return cpycnt;
}

/**
* @brief 读一段FIFO
*/
int32 kerFifo_Read(KERFIFO_HEAD* fifo,uint8 *pdata,int32 len)
{
    int32 cpycnt;
    if(fifo->magic != FIFI_MAGIC)
    {
        return 0;
    }
    for(cpycnt = 0; cpycnt < len; cpycnt++)
    {
        if(fifo->rdidx != fifo->wtidx)
        {
            pdata[cpycnt] = fifo->data[fifo->rdidx];
            fifo->rdidx = (fifo->rdidx + 1) % fifo->size;
        }
        else
        {
            //FIFO已经读取完成
            break;
        }
    }
    return cpycnt;
}
