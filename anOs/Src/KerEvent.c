
/**
* @brief os event module
* @author chenjl
* @date 2019.07.02
*/

#include "KerEvent.h"
#include "OsConfigure.h"
#include "string.h"

static KER_EVENT_STR ker_event_list[EVENT_NUM];

/**
* @brief 内核初始化时执行
*/
void kerEventModuleInit(void)
{
    int32 idx;
    int32 tidx;
    for(idx = 0;idx<EVENT_NUM;idx++)
    {
        ker_event_list[idx].eventName[0] = '\0';
        for(tidx = 0;tidx<MAXTASKNUM_PER_EVENT;tidx++)
        {
            ker_event_list[idx].waitingTasks[tidx] = 0;
        }
    }
}

/**
* @brief 创建一个事件
*/
int32 kerCreateEvent(char *name)
{
    int32 idx ;
    int32 emptyidx = -1;
    int32 handle = -1;
    for(idx = 0;idx<EVENT_NUM;idx++)
    {
        if(ker_event_list[idx].eventName[0] == '\0')
        {
            if(emptyidx < 0)
            {
                emptyidx = idx;
            }
        }
        else
        {
            if(strcmp(name,ker_event_list[idx].eventName) == 0)
            {
                //已经创建的信号量，直接返回该句柄
                handle = EVENT_HANDLE_FLAG | idx;
                return handle;
            }
        }
    }
    //需要新建一个句柄
    if(emptyidx >= 0)
    {
        strcpy(ker_event_list[emptyidx].eventName,name);
        for(idx=0;idx<MAXTASKNUM_PER_EVENT;idx++)
        {
            ker_event_list[emptyidx].waitingTasks[idx] = 0;
        }
        handle = EVENT_HANDLE_FLAG | emptyidx;
    }
    else
    {
        //没有空间了，
        handle = -1;        
    }
    return handle;
}

/**
* @brief 等待一个事件的发生
*/
int32 kerWaitEvent(int32 eventhandle,int32 timeout_ms)
{
    int32 realhandle = eventhandle & HANDLE_RELMASK;
    int32 idx;
    if(EVENT_HANDLE_FLAG != (eventhandle & HANDLE_MASK))
    {
        return -1;          //无效的句柄类型
    }
    if(realhandle >= EVENT_NUM)
    {
        return -2;          //无效的句柄值
    }
    for(idx=0;idx<MAXTASKNUM_PER_EVENT;idx++)
    {
        if(ker_event_list[realhandle].waitingTasks[idx] == 0)
        {
            ker_event_list[realhandle].waitingTasks[idx] = Kernel_GetCurTask();
            break;
        }
    }
    
    if(idx < MAXTASKNUM_PER_EVENT)
    {
        //成功添加等待件
        kerTaskWait(timeout_ms);
        return 0;
    }
    else
    {
        //等待队列已经满了，添加失败
        return  -3;
    }
}

/**
* @brief 通知一个事件发生
*/
void kerSetEvent(int32 eventhandle)
{
    int32 realhandle = eventhandle & HANDLE_RELMASK;
    int32 idx;
		uint8 waitPrior = 0;
	
    if(EVENT_HANDLE_FLAG != (eventhandle & HANDLE_MASK))
    {
        return;          //无效的句柄类型
    }
    if(realhandle >= EVENT_NUM)
    {
        return;          //无效的句柄值
    }
    for(idx=0;idx<MAXTASKNUM_PER_EVENT;idx++)
    {
        if(ker_event_list[realhandle].waitingTasks[idx] != 0)
        {
            if(ker_event_list[realhandle].waitingTasks[idx]->status == TASK_STATUS_WAIT)
            {
                ker_event_list[realhandle].waitingTasks[idx]->status = TASK_STATUS_READY;
                ker_event_list[realhandle].waitingTasks[idx]->wait_time = 0;
                if(ker_event_list[realhandle].waitingTasks[idx]->priority < waitPrior)
                {
                    waitPrior = ker_event_list[realhandle].waitingTasks[idx]->priority;
                }
            }
            ker_event_list[realhandle].waitingTasks[idx] = 0;
        }
    }
    
    /*如果触发事件的优先级高于当前任务的优先级，执行一次任务切换*/
    kerActiveHighPrior(waitPrior);
}
