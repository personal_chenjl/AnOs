
    AREA    |.text|, CODE, READONLY

PendSV_Handler		PROC
	EXPORT	PendSV_Handler
    IMPORT  Kernel_isSwitching
	IMPORT	Kernel_Active
        PUSH	{R0,LR}	
            
        ;判断是否需要切换上下文
        LDR     R2, = Kernel_isSwitching
        BLX		R2
        CMN     R0,#0
        BMI     endpend

        ;从此处开始，需要保存上下文 
        MRS     R1,PSP
        ;浮点上下文 
        VSTMDB	R1!,{S16-S31}
        ;通用上下文
        STMDB   R1!,{R4-R11}
        ;切换任务栈
        LDR     R2,=Kernel_Active
        BLX     R2
        
        ;从此以下，恢复新任务的栈空间
        ;恢复通用上下文 
        LDMIA	R0!,{R4-R11}			
        ;恢复浮点上下文
        VLDMIA	R0!,{S16-S31}
        ;恢复PSP栈
        MSR     PSP,R0
        ;返回任务执行
endpend   
        POP     {R0,PC}
        ENDP

SVC_Handler			PROC
	EXPORT	SVC_Handler
    IMPORT	SVC_Server
		TST     LR, #4      ;TEST BIT 2 OF EXC_RETURN
        ITE     EQ
        MRSEQ   R0,MSP
        MRSNE   R0,PSP
        B       SVC_Server
		ENDP
	END
			