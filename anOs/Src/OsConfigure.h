
/**
* @brief 操作系统参数配置文件
* @author chenjl
* @date 2019.07.01
*/

#ifndef __OSCONFIGURE_H__
#define __OSCONFIGURE_H__

#include "kerOs.h"
#include "Kernel.h"

#define     TASK_MAX_NUM    (7)
#define     TICK_PERIOD (500)  ///<单位，us

#define  	EVENT_NUM      (10)    ///<允许创建的最大事件数据

#define     MS_TO_TICK(ms)  (ms * 1000 / TICK_PERIOD)

#define     KERNEL_USE_MPU      (1)
#define		KERNEL_USE_PRIV		(1)		///< 用户模式使用私有指令


extern TASK_CONTEXT taskConfigurations[TASK_MAX_NUM];

extern void LoadConfiguration(void);
extern int32 task_init_num;

#endif
