
/**
* @brief 两个测试APP示例
* @details 请使用MultiTask中的示例程序，此处程序不应当被使用
* @author chenjl
* @date 2019.07.02
*/

#include "task.h"
#include "anOs.h"

void task1(void)
{
    int32 event = CreateEvent("event1");
    int32 devled = dev_open("LED",0);
    
    long t1_a = 0;
    float t1_b = 0;
    long tt2_a = 0;
    float tt2_b = 0;
    
    while(1)
    {            
        //WaitEvent(event,2000);
        t1_a++;
        t1_b += 1.0f;
        
        if(tt2_a != 0)
        {
            tt2_a = 1;
        }
        if(tt2_b != 0)
        {
            tt2_b = 1;
        }
        
        dev_write(devled,0,1);
        TaskWait(1000);
        dev_write(devled,0,0);
        TaskWait(1000);
    }
}

#include "stdarg.h"

void task2(void)
{
    int32 event = CreateEvent("event1");
    int32 uartport = dev_open("COM1",115200);
    
    long tt1_a = 0;
    float tt1_b = 0;
    long t2_a = 0;
    float t2_b = 0;
    
    while(1)
    {
        if(t2_a != 0)
        {
            t2_a = 1;
        }
        if(t2_b != 0)
        {
            t2_b = 1;
        }
        
        tt1_a+=2;
        tt1_b += 2.0f;
        
        t2_a = GetCpuPayload();
        
        //TaskWait(1000);
        //SetEvent(event);   
        dev_write(uartport,"abcd\r\n",6);
        TaskWaitNxtPeriod();
    }
}
