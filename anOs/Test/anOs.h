
/**
* @brief 操作系统内核头文件
* @author chenjl
* @date 2019.07.01
*/

#ifndef __ANOS_H__
#define __ANOS_H__

#include "stm32f4xx.h"

typedef unsigned long  uint32;
typedef signed   long  int32;
typedef unsigned short uint16;
typedef signed   short int16;
typedef unsigned char  uint8;
typedef signed   char  int8;

/**
* @brief 向应用程序提供的接口函数定义
*/
int32 dev_open(char* name,uint32 param);
int32 dev_read(int32 handle,void *pdata,int32 len);
int32 dev_write(int32 handle,void *pdata,int32 len);
int32 dev_ctrl(int32 handle,uint32 code,uint32 value);

void Kill(void);
void TaskWait(uint32 ms);
void TaskWaitNxtPeriod(void);
void TaskDelay(uint32 us);
void TaskKill(void);

uint32 GetSystemTimeMs(void);
uint32 GetCpuPayload(void);
int32 CreateEvent(char* name);
void WaitEvent(int32 handle,int32 timeout);
void SetEvent(int32 handle);

#endif
