/**
* @brief 分区应用示例2
* @author chenjl
* @date 2019.07.01
*/

#include "anOs.h"

int app_var_1 = 2;

int app_data  = 3;
char app_name[20] = "this_is_app_string";

void AppMain(void)
{
    int32 uartport = dev_open("COM1",115200);
    
    long tt1_a = 0;
    float tt1_b = 0;
    long t2_a = 0;
    float t2_b = 0;
    
    while(1)
    {
        if(t2_a != 0)
        {
            t2_a = 1;
        }
        if(t2_b != 0)
        {
            t2_b = 1;
        }
        
        tt1_a+=2;
        tt1_b += 2.0f;
        
        t2_a = GetCpuPayload();

        dev_write(uartport,"abcd\r\n",6);
        TaskWaitNxtPeriod();
    }
}
